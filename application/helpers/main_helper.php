<?php

    function check_nilai($cur, $max = 0)
    {
        $thumbs_down = '<span class="text-alizarin"><i class="fa fa-thumbs-down"></i></span>&nbsp;';
        $thumbs_up = '<span class="text-emerland"><i class="fa fa-thumbs-up"></i></span>&nbsp;';
        $star_green = '<span class="text-emerland"><i class="fa fa-star"></i></span>&nbsp;';
        $star_gold = '<span class="text-sunflower"><i class="fa fa-star"></i></span>&nbsp;';

        $total_nilai = '';
        if ($max) {
            $temp = ($cur / $max) * 100;
        } else {
            $temp = $cur;
        }
        if ($temp) {
            switch ($temp) {
            case $temp > 0 && $temp <= 2:
                $total_nilai = $thumbs_up;
                break;
            case $temp > 2 && $temp <= 5:
                $total_nilai = str_repeat($thumbs_up, 2);
                break;
            case $temp > 5 && $temp <= 8:
                $total_nilai = str_repeat($thumbs_up, 3);
                break;
            case $temp > 8 && $temp <= 12:
                $total_nilai = str_repeat($thumbs_up, 4);
                break;
            case $temp > 12 && $temp <= 17:
                $total_nilai = str_repeat($thumbs_up, 5);
                break;
            case $temp > 17 && $temp <= 23:
                $total_nilai = $star_green;
                break;
            case $temp > 23 && $temp <= 30:
                $total_nilai = str_repeat($star_green, 2);
                break;
            case $temp > 30 && $temp <= 37:
                $total_nilai = str_repeat($star_green, 3);
                break;
            case $temp > 37 && $temp <= 45:
                $total_nilai = str_repeat($star_green, 4);
                break;
            case $temp > 45 && $temp <= 54:
                $total_nilai = str_repeat($star_green, 5);
                break;
            case $temp > 54 && $temp <= 64:
                $total_nilai = $star_gold;
                break;
            case $temp > 64 && $temp <= 75:
                $total_nilai = str_repeat($star_gold, 2);
                break;
            case $temp > 75 && $temp <= 87:
                $total_nilai = str_repeat($star_gold, 3);
                break;
            case $temp > 87 && $temp <= 99:
                $total_nilai = str_repeat($star_gold, 4);
                break;
            case $temp > 99 && $temp <= 100:
                $total_nilai = str_repeat($star_gold, 5);
                break;
            }
        } else {
            if ($max) {
                $total_nilai = $thumbs_down;
            } else {
                $total_nilai = '';
            }
        }

        return $total_nilai;
    }

    function check_nilaitotal($nilai)
    {
        $thumbs_down = '<span class="text-alizarin"><i class="fa fa-thumbs-down"></i></span>&nbsp;';
        $thumbs_up = '<span class="text-emerland"><i class="fa fa-thumbs-up"></i></span>&nbsp;';
        $star_green = '<span class="text-emerland"><i class="fa fa-star"></i></span>&nbsp;';
        $star_gold = '<span class="text-sunflower"><i class="fa fa-star"></i></span>&nbsp;';

        $total_nilai = '';
        if ($nilai > 0) {
            switch ($nilai) {
            case $nilai > 0 && $nilai <= 5:
                $total_nilai = $thumbs_up;
                break;
            case $nilai > 5 && $nilai <= 30:
                $total_nilai = str_repeat($thumbs_up, 2);
                break;
            case $nilai > 30 && $nilai <= 70:
                $total_nilai = str_repeat($thumbs_up, 3);
                break;
            case $nilai > 70 && $nilai <= 134:
                $total_nilai = str_repeat($thumbs_up, 4);
                break;
            case $nilai > 134 && $nilai <= 236:
                $total_nilai = str_repeat($thumbs_up, 5);
                break;
            case $nilai > 236 && $nilai <= 402:
                $total_nilai = $star_green;
                break;
            case $nilai > 402 && $nilai <= 669:
                $total_nilai = str_repeat($star_green, 2);
                break;
            case $nilai > 669 && $nilai <= 1100:
                $total_nilai = str_repeat($star_green, 3);
                break;
            case $nilai > 1100 && $nilai <= 1797:
                $total_nilai = str_repeat($star_green, 4);
                break;
            case $nilai > 1797 && $nilai <= 2924:
                $total_nilai = str_repeat($star_green, 5);
                break;
            case $nilai > 2924 && $nilai <= 4747:
                $total_nilai = $star_gold;
                break;
            case $nilai > 4747 && $nilai <= 7696:
                $total_nilai = str_repeat($star_gold, 2);
                break;
            case $nilai > 7696 && $nilai <= 12465:
                $total_nilai = str_repeat($star_gold, 3);
                break;
            case $nilai > 12465 && $nilai <= 20181:
                $total_nilai = str_repeat($star_gold, 4);
                break;
            case $nilai > 20181:
                $total_nilai = str_repeat($star_gold, 5);
                break;
            }
        } else {
            $total_nilai = $thumbs_down;
        }

        return $total_nilai;
    }

    function get_month_name($month, $initial = 0)
    {
        switch ($month) {
            case '01':
                $list = 'Januari';
                break;
            case '02':
                $list = 'Februari';
                break;
            case '03':
                $list = 'Maret';
                break;
            case '04':
                $list = 'April';
                break;
            case '05':
                $list = 'Mei';
                break;
            case '06':
                $list = 'Juni';
                break;
            case '07':
                $list = 'Juli';
                break;
            case '08':
                $list = 'Agustus';
                break;
            case '09':
                $list = 'September';
                break;
            case '10':
                $list = 'Oktober';
                break;
            case '11':
                $list = 'November';
                break;
            case '12':
                $list = 'Desember';
                break;
        }
        if ($initial) {
            if ($cur == '08') {
                $list = 'Ags';
            } else {
                $list = substr($list, 0, 3);
            }
        }

        return $list;
    }
	function list_bulan(){
		$data=array(
					array( 'bulan_id'    => '01',
					'namabulan'  => 'Januari'),
					array( 'bulan_id'    => '02',
					'namabulan'  => 'Febuari'),
					array( 'bulan_id'    => '03',
					'namabulan'  => 'Maret'),
					array( 'bulan_id'    => '04',
					'namabulan'  => 'April'),
					array( 'bulan_id'    => '05',
					'namabulan'  => 'Mei'),
					array( 'bulan_id'    => '06',
					'namabulan'  => 'Juni'),
					array( 'bulan_id'    => '07',
					'namabulan'  => 'Juli'),
					array( 'bulan_id'    => '08',
					'namabulan'  => 'Agustus'),
					array( 'bulan_id'    => '09',
					'namabulan'  => 'September'),
					array( 'bulan_id'    => '10',
					'namabulan'  => 'Oktober'),
					array( 'bulan_id'    => '11',
					'namabulan'  => 'November'),
					array( 'bulan_id'    => '12',
					'namabulan'  => 'Desember'),
		);
		return $data;
	}
	function list_tahun(){
		$data=array();
		$index=0;
		for($th=date('y');$th >= 16;$th--){
			// $data[]=$th;
			$data[]=array( 'tahun_id'    => $th,'namatahun'  => '20'.$th);
		}

		return $data;
	}
	function max_page($total_rows,$per_page){
		return ceil($total_rows/$per_page);
	}
	function get_offset($page,$limit){
		if ($page==1){
			return 0;
		}else{
			return (($page-1)*$limit);
		}

	}
	function get_pagination($akhir){
		$data=array();
		for ($i = 1; $i <= $akhir; ++$i) {
            $data[] = $i;
        }
		return $data;
	}
	function get_roymp14($cur){
	switch ($cur)
		{
		case 1:
			$clas = "Director";
			break;
		case 2:
			$clas = "Senior Manager";
			break;
		case 3:
			$clas = "Group Manager";
			break;
		case 4:
			$clas = "Unit Manager";
			break;
		default:
			$clas = 0;
			break;
		}

		return $clas;
	}
	function ec_rstpwd($name,$noid,$password){
		$mailcontent='<p>Yth '.$name.'</p>';
		$mailcontent.='<p style="align:justify;" >Sesuai permintaan anda untuk mereset password login anda di ';
		$mailcontent.='<a href="http://tridayasinergi.co" target="_blank">';
		$mailcontent.='Login Member Tridaya Sinergi</a>, untuk itu kami telah mereset ulang password anda.<br />';
		$mailcontent.='Rincian login anda sekarang adalah sebagai berikut:</p>';
		$mailcontent.='<table border="0" cellspacing="2" cellpadding="2" style="margin-left:40px">';
		$mailcontent.='<tr><td width="110">NoID</td><td width="400">:&nbsp; '.$noid.'</td>';
		$mailcontent.='</tr><tr><td width="110">Password</td><td width="400">:&nbsp; '.$password.'</td>';
		$mailcontent.='</tr></table><p style="align:justify;" >Kami sarankan anda untuk mengubah password Anda secepatnya, ';
		$mailcontent.='menjadi password yang mudah Anda ingat,<br /> ';
		$mailcontent.='Untuk merubahnya silahkan Login dan klik &quot;Setting&quot; -> &quot;Ganti Password&quot;.</p>';
		$mailcontent.='<p>Salam Sejahtera,<br /><a href="http://tridayasinergi.com" target="_blank">TridayaSinergi.com</a></p>';
		$mailcontent.='<p style="border:1px solid #FF0000; padding:5px"><strong>Pengumuman:</strong> <br/> 
                       Kami menghimbau kepada Seluruh Mitra Stokis/ Leader untuk dapat menyampaikan informasi kepada Mitranya tentang Kenaikan Harga Produk SIN TSI tahap 2.<br/>  
                       Berlaku mulai tanggal <strong>20 April 2016</strong>.  Info lebih lengkap kunjungi Website <a target="_blank" href="http://www.tridayasinergi.com/_tsi-news-detail.php?id=42">www.tridayasinergi.com</a></p>';
				
		return $mailcontent; 
	}
