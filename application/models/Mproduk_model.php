<?php

class Mproduk_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_category()
    {
        $this->db->select('tipeproduk AS kategori');
        $this->db->group_by('tipeproduk');
        $query = $this->db->get('manggotaproduk');

        return $query->result();
    }

    public function get_all($sthotdeals)
    {
        $this->db->select('manggotaproduk.*, CONCAT("https://partner.kulcimart.com/assets/gambarProduk/", manggotaproduk.foto) AS foto');
        $this->db->where('sttampilhotdeals', $sthotdeals);
        $query = $this->db->get('manggotaproduk');

        return $query->result();
    }

    public function get_specified_entries($sthotdeals, $idproduk)
    {
        $this->db->select('manggotaproduk.*, CONCAT("https://partner.kulcimart.com/assets/gambarProduk/", manggotaproduk.foto) AS foto');
        $this->db->where('sttampilhotdeals', $sthotdeals);
        $this->db->where('idproduk', $idproduk);
        $query = $this->db->get('manggotaproduk');

        return $query->result();
    }
}
