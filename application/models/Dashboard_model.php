<?php

class Dashboard_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_count_salejalur($noid, $periode)
    {
        $this->db->select('personal,salejalur1,salejalur2,salejalur3,salejalur4,salejalur5');
        $this->db->from('mkomisi');
        $this->db->where('periode', $periode);
        $this->db->where('noid', $noid);
        $query = $this->db->get();

        return $query->row();
    }

    public function get_count_peraih_um($periode)
    {
        $this->db->select('mkomisiroyaltium.*');
        $this->db->from('mkomisiroyaltium');
        $this->db->where('periode', $periode);

        return $this->db->count_all_results();
    }

    public function get_count_peraih_gm($periode)
    {
        $this->db->select('mkomisiroyaltigm.*');
        $this->db->from('mkomisiroyaltigm');
        $this->db->where('periode', $periode);

        return $this->db->count_all_results();
    }

    public function get_count_peraih_sm($periode)
    {
        $this->db->select('mkomisiroyaltism.*');
        $this->db->from('mkomisiroyaltism');
        $this->db->where('periode', $periode);

        return $this->db->count_all_results();
    }

    public function get_count_peraih_di($periode)
    {
        if ($periode >= date('ym')) {
            $this->db->select('mkomisiroyalti.*');
            $this->db->from('mkomisiroyalti');
            $this->db->where('periode', $periode);

            return $this->db->count_all_results();
        } else {
            $this->db->select('mkomisiroyalti_pu.*');
            $this->db->from('mkomisiroyalti_pu');
            $this->db->where('periode', $periode);

            return $this->db->count_all_results();
        }
    }

    public function get_count_peraih_pd($periode)
    {
        if ($periode >= date('ym')) {
            $this->db->select('mkomisiroyalti.*');
            $this->db->from('mkomisiroyalti');
            $this->db->where('periode', $periode);
            $this->db->where('mkomisiroyalti.salejalur1 >=', 5000);
            $this->db->where('mkomisiroyalti.salejalur2 >=', 5000);
            $this->db->where('mkomisiroyalti.salejalur3 >=', 5000);
            $this->db->where('mkomisiroyalti.salejalur4 >=', 5000);
            $this->db->where('mkomisiroyalti.salejalur5 >=', 5000);

            return $this->db->count_all_results();
        } else {
            $this->db->select('mkomisiroyalti_pu.*');
            $this->db->from('mkomisiroyalti_pu');
            $this->db->where('periode', $periode);
            $this->db->where('mkomisiroyalti_pu.salejalur1 >=', 5000);
            $this->db->where('mkomisiroyalti_pu.salejalur2 >=', 5000);
            $this->db->where('mkomisiroyalti_pu.salejalur3 >=', 5000);
            $this->db->where('mkomisiroyalti_pu.salejalur4 >=', 5000);
            $this->db->where('mkomisiroyalti_pu.salejalur5 >=', 5000);

            return $this->db->count_all_results();
        }
    }

    //get peraih unit manager
    public function get_peraih_um($periode)
    {
        $this->db->select('mkomisiroyaltium.*,mmembers.namamembers,(mkomisiroyaltium.salejalur1+mkomisiroyaltium.salejalur2+mkomisiroyaltium.salejalur3+mkomisiroyaltium.salejalur4+mkomisiroyaltium.salejalur5) as total');
        $this->db->from('mkomisiroyaltium');
        $this->db->join('mmembers', 'mmembers.noid = mkomisiroyaltium.noid');
        $this->db->where('periode', $periode);
        $this->db->order_by('mkomisiroyaltium.timestamp', 'ASC');
        $this->db->limit(5);
        $query = $this->db->get();

        return $query->result_array();
    }

    //get peraih general manager
    public function get_peraih_gm($periode)
    {
        $this->db->select('mkomisiroyaltigm.*,mmembers.namamembers,(mkomisiroyaltigm.salejalur1+mkomisiroyaltigm.salejalur2+mkomisiroyaltigm.salejalur3+mkomisiroyaltigm.salejalur4+mkomisiroyaltigm.salejalur5) as total');
        $this->db->from('mkomisiroyaltigm');
        $this->db->join('mmembers', 'mmembers.noid = mkomisiroyaltigm.noid');
        $this->db->order_by('mkomisiroyaltigm.timestamp', 'ASC');
        $this->db->where('periode', $periode);
        $this->db->limit(5);
        $query = $this->db->get();

        return $query->result_array();
    }

    //get peraih senior manager
    public function get_peraih_sm($periode)
    {
        $this->db->select('mkomisiroyaltism.*,mmembers.namamembers,(mkomisiroyaltism.salejalur1+mkomisiroyaltism.salejalur2+mkomisiroyaltism.salejalur3+mkomisiroyaltism.salejalur4+mkomisiroyaltism.salejalur5) as total');
        $this->db->from('mkomisiroyaltism');
        $this->db->join('mmembers', 'mmembers.noid = mkomisiroyaltism.noid');
        $this->db->order_by('mkomisiroyaltism.timestamp', 'ASC');
        $this->db->where('periode', $periode);
        $this->db->limit(5);
        $query = $this->db->get();

        return $query->result_array();
    }

    //get peraih director
    public function get_peraih_di($periode)
    {
        if ($periode >= date('ym')) {
            $this->db->select('mkomisiroyalti.*,mmembers.namamembers,(mkomisiroyalti.salejalur1+mkomisiroyalti.salejalur2+mkomisiroyalti.salejalur3+mkomisiroyalti.salejalur4+mkomisiroyalti.salejalur5) as total');
            $this->db->from('mkomisiroyalti');
            $this->db->join('mmembers', 'mmembers.noid = mkomisiroyalti.noid');
            $this->db->order_by('mkomisiroyalti.timestamp', 'ASC');
            $this->db->where('periode', $periode);
            $this->db->limit(5);
            $query = $this->db->get();

            return $query->result_array();
        } else {
            $this->db->select('mkomisiroyalti_pu.*,mmembers.namamembers,(mkomisiroyalti_pu.salejalur1+mkomisiroyalti_pu.salejalur2+mkomisiroyalti_pu.salejalur3+mkomisiroyalti_pu.salejalur4+mkomisiroyalti_pu.salejalur5) as total');
            $this->db->from('mkomisiroyalti_pu');
            $this->db->join('mmembers', 'mmembers.noid = mkomisiroyalti_pu.noid');
            $this->db->order_by('total', 'DESC');
            $this->db->where('periode', $periode);
            ($limit == '') ? $this->db->limit($offset, 0) : $this->db->limit($offset, $limit);
            $query = $this->db->get();

            return $query->result_array();
        }
    }

    //get peraih president director
    public function get_peraih_pd($periode)
    {
        if ($periode >= date('ym')) {
            $this->db->select('mkomisiroyalti.*,mmembers.namamembers,(mkomisiroyalti.salejalur1+mkomisiroyalti.salejalur2+mkomisiroyalti.salejalur3+mkomisiroyalti.salejalur4+mkomisiroyalti.salejalur5) as total');
            $this->db->from('mkomisiroyalti');
            $this->db->join('mmembers', 'mmembers.noid = mkomisiroyalti.noid');
            $this->db->order_by('mkomisiroyalti.timestamp', 'ASC');
            $this->db->where('periode', $periode);
            $this->db->where('mkomisiroyalti.salejalur1 >=', 5000);
            $this->db->where('mkomisiroyalti.salejalur2 >=', 5000);
            $this->db->where('mkomisiroyalti.salejalur3 >=', 5000);
            $this->db->where('mkomisiroyalti.salejalur4 >=', 5000);
            $this->db->where('mkomisiroyalti.salejalur5 >=', 5000);
            $this->db->limit(5);
            $query = $this->db->get();

            return $query->result_array();
        } else {
            $this->db->select('mkomisiroyalti_pu.*,mmembers.namamembers,(mkomisiroyalti_pu.salejalur1+mkomisiroyalti_pu.salejalur2+mkomisiroyalti_pu.salejalur3+mkomisiroyalti_pu.salejalur4+mkomisiroyalti_pu.salejalur5) as total');
            $this->db->from('mkomisiroyalti_pu');
            $this->db->join('mmembers', 'mmembers.noid = mkomisiroyalti_pu.noid');
            $this->db->order_by('total', 'DESC');
            $this->db->where('periode', $periode);
            $this->db->where('mkomisiroyalti_pu.salejalur1 >=', 5000);
            $this->db->where('mkomisiroyalti_pu.salejalur2 >=', 5000);
            $this->db->where('mkomisiroyalti_pu.salejalur3 >=', 5000);
            $this->db->where('mkomisiroyalti_pu.salejalur4 >=', 5000);
            $this->db->where('mkomisiroyalti_pu.salejalur5 >=', 5000);
            ($limit == '') ? $this->db->limit($offset, 0) : $this->db->limit($offset, $limit);
            $query = $this->db->get();

            return $query->result_array();
        }
    }
}
