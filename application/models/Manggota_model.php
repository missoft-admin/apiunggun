<?php

class Manggota_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all($tipeanggota)
    {
        $this->db->where('tipeanggota', $tipeanggota);
        $query = $this->db->get('manggota');

        return $query->result();
    }

    public function get_specified_entries($tipeanggota, $idanggota)
    {
        $this->db->where('tipeanggota', $tipeanggota);
        $this->db->where('idanggota', $idanggota);
        $query = $this->db->get('manggota');

        return $query->result();
    }
}
