<?php

class Minfo_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_about()
    {
        $query = $this->db->get('mtentangkami');

        return $query->result();
    }
}
