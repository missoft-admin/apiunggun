<?php

class Mproduk extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mproduk_model');
    }

    public function index()
    {
        echo 'Directory access is forbidden';
    }

    // Data Kategori
    public function get_category()
    {
        $data = $this->mproduk_model->get_category();
        if($data){
          $response = array(
              'status' => 'SUCCESS',
              'data' => $data,
          );
        }else{
          $response = array(
              'status' => 'ERROR',
              'data' => '',
          );
        }

        $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }

    // Data Produk
    public function get_all($sthotdeals)
    {
        $data = $this->mproduk_model->get_all($sthotdeals);
        if($data){
          $response = array(
              'status' => 'SUCCESS',
              'data' => $data,
          );
        }else{
          $response = array(
              'status' => 'ERROR',
              'data' => '',
          );
        }

        $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }

    public function get_specified_entries($sthotdeals, $idproduk)
    {
        $data = $this->mproduk_model->get_specified_entries($sthotdeals, $idproduk);
        if($data){
          $response = array(
              'status' => 'SUCCESS',
              'data' => $data,
          );
        }else{
          $response = array(
              'status' => 'ERROR',
              'data' => '',
          );
        }

        $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
