<?php

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('dashboard_model');
    }

    public function index()
    {
        echo 'Directory access is forbidden';
    }

    public function get_count_peraih_royalti()
    {
        $periode = date('ym');
        $response = array(
            'count_peraih_um' => $this->dashboard_model->get_count_peraih_um($periode),
            'count_peraih_gm' => $this->dashboard_model->get_count_peraih_gm($periode),
            'count_peraih_sm' => $this->dashboard_model->get_count_peraih_sm($periode),
            'count_peraih_di' => $this->dashboard_model->get_count_peraih_di($periode),
            'count_peraih_pd' => $this->dashboard_model->get_count_peraih_pd($periode),
        );

        $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }

    public function get_list_peraih_royalti()
    {
        $periode = date('ym');
        $response = array(
            'peraih_um' => $this->dashboard_model->get_peraih_um($periode),
            'peraih_gm' => $this->dashboard_model->get_peraih_gm($periode),
            'peraih_sm' => $this->dashboard_model->get_peraih_sm($periode),
            'peraih_di' => $this->dashboard_model->get_peraih_di($periode),
            'peraih_pd' => $this->dashboard_model->get_peraih_pd($periode),
        );

        $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }

    public function get_count_salejalur($noid)
    {
        $periode_saatini = date('ym');
        $periode_sebelum = date('ym', strtotime(date('Y-m-d', strtotime(date('Y-m-01'))).'-1 month'));

        if ($row = $this->dashboard_model->get_count_salejalur(strtoupper($noid), $periode_saatini)) {
            $personal = $row->personal;
            $salejalur1 = $row->salejalur1;
            $salejalur2 = $row->salejalur2;
            $salejalur3 = $row->salejalur3;
            $salejalur4 = $row->salejalur4;
            $salejalur5 = $row->salejalur5;
        } else {
            $personal = 0;
            $salejalur1 = 0;
            $salejalur2 = 0;
            $salejalur3 = 0;
            $salejalur4 = 0;
            $salejalur5 = 0;
        }
        if ($row = $this->dashboard_model->get_count_salejalur(strtoupper($noid), $periode_sebelum)) {
            $salejalurb1 = $row->salejalur1;
            $salejalurb2 = $row->salejalur2;
            $salejalurb3 = $row->salejalur3;
            $salejalurb4 = $row->salejalur4;
            $salejalurb5 = $row->salejalur5;
        } else {
            $salejalurb1 = 0;
            $salejalurb2 = 0;
            $salejalurb3 = 0;
            $salejalurb4 = 0;
            $salejalurb5 = 0;
        }

        $response = array(
            'class_salejalur1' => $this->get_clases($salejalur1, $salejalurb1 * 0.5),
            'class_salejalur2' => $this->get_clases($salejalur2, $salejalurb2 * 0.5),
            'class_salejalur3' => $this->get_clases($salejalur3, $salejalurb3 * 0.5),
            'class_salejalur4' => $this->get_clases($salejalur4, $salejalurb4 * 0.5),
            'class_salejalur5' => $this->get_clases($salejalur5, $salejalurb5 * 0.5),

            'count_salejalur1' => $salejalur1,
            'count_salejalur2' => $salejalur2,
            'count_salejalur3' => $salejalur3,
            'count_salejalur4' => $salejalur4,
            'count_salejalur5' => $salejalur5,
        );

        $this->output
        ->set_status_header(201)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }

    public function get_clases($salejalur, $salejalurb)
    {
        if ($salejalur && $salejalurb) {
            $class = $this->get_range($salejalur, $salejalurb);
        } elseif ($salejalur && $salejalurb == 0) {
            $class = 'highlighted bg-orange-dark';
        } elseif ($salejalur == 0) {
            $class = 'bg-midnightblue';
        }

        return $class;
    }

    public function get_range($cur, $before)
    {
        switch ($cur) {
        case $cur >= ($before * 3):
            $class = 'bg-belizehole';
            break;
        case $cur >= ($before * 2) && $cur < ($before * 3):
            $class = 'bg-nephritis';
            break;
        case $cur >= ($before) && $cur < ($before * 2):
            $class = 'bg-orange';
            break;
        case $cur >= 1 && $cur < ($before):
            $class = 'bg-pomegranate';
            break;
        default:
            $class = 'bg-midnightblue';
            break;
        }

        return $class;
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
