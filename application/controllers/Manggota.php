<?php

class Manggota extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('manggota_model');
    }

    public function index()
    {
        echo 'Directory access is forbidden';
    }

    // Data Anggota
    public function get_all($tipeanggota)
    {
        $data = $this->manggota_model->get_all($tipeanggota);
        if($data){
          $response = array(
              'status' => 'SUCCESS',
              'data' => $data,
          );
        }else{
          $response = array(
              'status' => 'ERROR',
              'data' => '',
          );
        }

        $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }

    public function get_specified_entries($tipeanggota, $idanggota)
    {
        $data = $this->manggota_model->get_specified_entries($tipeanggota, $idanggota);
        if($data){
          $response = array(
              'status' => 'SUCCESS',
              'data' => $data,
          );
        }else{
          $response = array(
              'status' => 'ERROR',
              'data' => '',
          );
        }

        $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
