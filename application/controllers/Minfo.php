<?php

class Minfo extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('minfo_model');
    }

    public function index()
    {
        echo 'Directory access is forbidden';
    }

    // Data Tentang Kami
    public function get_about()
    {
        $data = $this->minfo_model->get_about();
        if($data){
          $response = array(
              'status' => 'SUCCESS',
              'data' => $data,
          );
        }else{
          $response = array(
              'status' => 'ERROR',
              'data' => '',
          );
        }

        $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($response, JSON_PRETTY_PRINT))
        ->_display();
        exit;
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
